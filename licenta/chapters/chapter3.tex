\chapter{Comparing the API Implementations}

We now look over the differences between the old API and our proposed API.
This comparison helps the understanding of why this new implementation is
beneficial for \textbf{vmchecker}.

We examine how authorization is carried out in both implementations and present
the trade-off between session-based authorization and HTTP Basic Auth.

We also look at differences between the way resources are used in both
implementations and how the RESTful approach makes use of HTTP verbs
and status codes to provide a intuitive interface for the client.

\section{Authorization}

The old API relies on HTTP session. Each client calls
the login method to get authorization. If the method is successful it
will mark the session as authorized and all the subsequent requests
will need to add a cookie to reference this session. This approach
relies on support from the server and takes up memory while the session
is active because the server must keep the session around. This provides
for a good experience for the user interface as a user does not have
to constantly log into the application.

Because a RESTful API needs to be stateless, we can't rely on context from
the server to check for authorization and we just provide it with every request.

\section{Resources}

Except for one resource, the old API has no methods that can create or modify
resources. Every modification must be made directly to configuration files.

The new API provides an endpoint for each resource and HTTP verbs are used
to determine which action will be taken for the current resource:
\begin{itemize}
    \item PATCH for editing
    \item POST for adding
    \item DELETE (this is self-documenting)
\end{itemize}

\subsection{Submissions}

Both APIs can handle new submissions (which is understandable because this is
pretty much the point of vmchecker).

The old API can only handle new submissions. This is done through
form/multipart-data requests. Each request uploads a new assignment to
the server. Grading is done by navigating to the submission folder and
editing a file called \textbf{grade.vmr}.

The new API handles new submissions pretty much the same as the old one.
JSON is no longer used for the payload data. The reason we do it like this is
to support a wider range of clients. All HTTP clients must know how to send
multipart/form-data requests. Using an approach based on JSON would have
meant that the client must implement it on the client side and this might
introduce complexity to the API implementation.

However, the new API also handles grading a submission (which is simply
editing the submission grade and comments). The time limit between
submission is also easier to implement because we timestamp each submission
and when a new one arrives, we simply subtract the timestamps and check to
see if the submission is allowed. In the old API, this logic was handled
inside the core which would raise an exception if a submission was uploaded
too soon.

\subsection{Getting Resources}

The old API only had support for getting courses, assignments and submission
grades. The new API adds support for users, testers, storers, virtual machines.

The difference between APIs stems from the design of the clients.
The old API was created to interact only with the web interface. As such
it only returns a subset of the information because this is simply all that
is needed for the web interface.

The new API is designed to be used by more than just a web interface.
It provides all the information and lets the client choose what he needs rather
than making assumptions in his stead.

\clearpage
\subsection{A Head to Head Comparison}

Table \ref{tbl:api_comparison01} summarizes the essential differences between
the old and the new API.

\begin{table}[h!]
    \begin{tabular}{r|p{4.5cm}|p{6.5cm}|}
        \cline{2-3}
        & \multicolumn{2}{c|}{Implementations} \\
        \cline{2-3}
        & Old API & New API \\
        \cline{1-3}
        \multicolumn{1}{|r|}{Authorization} &
            Session-based authorization & HTTP Basic Auth \\
        \cline{1-3}
        \multicolumn{1}{|r|}{Endpoints} &
            An endpoint for every available method &
            An endpoint for every available resource \\
        \cline{1-3}
        \multicolumn{1}{|r|}{Available resources} &
                Course (get) \newline
                Assignment (get) \newline
                Submit (add, get grade) &
                Course (add, edit, get, delete) \newline
                Assignment (add, edit, get, delete) \newline
                Submit (add, edit, get) \newline
                Holiday (add, edit, get, delete) \newline
                User (add, edit, get, delete) \newline
                Storer (add, edit, get, delete) \newline
                Tester (add, edit, get, delete) \newline
                Machine (add, edit, get, delete) \\
        \cline{1-3}
    \end{tabular}
    \caption{Summary of the differences between the API implementations}
    \label{tbl:api_comparison01}
\end{table}




