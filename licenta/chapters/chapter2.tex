\chapter{Designing the RESTful API}

Building the API using the current design means adding a RPC
\footnote{Remote Procedure Call} endpoint for every method added.
This translated into a separate URL for each method.

With a RESTful API, we define a set of resources and we only introduce HTTP
endpoints for each resource rather than each action available for it which makes
for an easier way to learn and use an API (once you know how to use an
endpoint, the other endpoints have the same semantics).

Further on, we look at building a RESTful API using Flask.

\section{Motivation for Flask}

Flask is a micro-framework based on Werkzeug which is a WSGI Utility Library.
It provides an easy and straightforward way of writing web applications
that can then be run on top of any WSGI servers.

Listing \ref{lst:flask01} shows an example of a simple web application.
\begin{listing}
    \begin{minted}[linenos=true]{python}
from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"

    if __name__ == "__main__":
        app.run()
    \end{minted}
    \caption{Simple web application using Flask}
    \label{lst:flask01}
\end{listing}

Flask abstracts away much of the complexity needed
to implement a web service and provides simple and intuitive ways to handle
routing and request handling.

Flask also provides a simple, single-threaded web server that is very helpful
while prototyping and testing available functionality.

\section{Motivation for REST}

"REST is an architectural style"\cite{fielding2000} used by an increasing number
of online web services that aim to provide external APIs for developers
(such as GitHub\footnote{https://developer.github.com/v3/}).

\subsection{Uniform Interface}

The first thing our API needs to do to be REST compliant is to use
URL (Uniform Resource Locator) for resource identification. Actions
that a certain resource supports are represented by HTTP verbs.

For example, getting all the available courses are done by sending
a HTTP GET request to the \texttt{api/courses} endpoint. Adding
a new course is done by sending a HTTP POST to the same endpoint.

This makes each resource resemble a class that needs to support
the following methods: get, post, put, patch and delete.

In Flask, this can easily be done by using a concept named
\textbf{pluggable views}.
\begin{listing}
    \begin{minted}[linenos=true]{python}
from flask.views import MethodView

def Resource(MethodView):
    def get(self, resource_id):
        pass
    def post(self):
        pass
    def put(self, resource_id):
        pass
    def patch(self, resource_id):
        pass
    def delete(self, resource_id):
        pass
    \end{minted}
    \caption{Resource definition in Flask}
    \label{lst:flask02}
\end{listing}

Each such view is exposed as an API endpoint and represents a single
resource.

\subsection{Internal Resource Representation}

Right now, \textbf{vmchecker} resources are stored in the following way:
\begin{itemize}
    \item
        course configuration files contain the assignment, storer and
        tester configuration.
    \item
        user configuration is either pulled from LDAP\footnote{Lightweight
        Directory Access Protocol} or from a JSON
        configuration file.
\end{itemize}

There are some problems with this approach:
\begin{itemize}
    \item it lacks structure
    \begin{itemize}
        \item the order of the configuration options is not important
        \item unknown or misspelled options are ignored and default
            values may silently be provided for them
    \end{itemize}
    \item it promotes duplication
    \begin{itemize}
        \item each course must define a virtual machine configuration
            even though multiple courses might use the same one.
    \end{itemize}
\end{itemize}

That is why we need to move run-time configuration from files to a
relational database management system (RDBMS).
Since queries are fairly simple we can use an object-relational mapping
approach to abstract away database implementations. The ORM toolkit is in
charge of generating the database query and this greatly simplifies the
way resources are used internally.

Using database backed resources enables the use of foreign keys and unique
constraints. Database schemas are self-documenting and enforce constraints
so a well thought-out schema will, by design, reject invalid configuration
options.

The ORM toolkit of choice is
SQLAlchemy\footnote{http://docs.sqlalchemy.org/en/latest/index.html}.
A database table is defined in the following way:
\begin{listing}
    \begin{minted}[linenos=true]{python}
from sql.declarative import Column, Integer, String
from sql.ext.declarative import declarative_base

Base = declarative_base()

class Course(Base):
    __tablename__ = 'courses'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String, nullable=False)
    \end{minted}
    \caption{Database modeling using SQLAlchemy}
    \label{lst:sqlalchemy01}
\end{listing}

Each record of the above table is simply an instance of the above class and
this is transparent for the code. Any implementation which would map
resources to classes in this way could easily replace the current solution
with no additional code.

\subsection{External Resource Representation}

How do clients see resources? We propose two solutions:
\begin{itemize}
    \item JSON\footnote{JavaScript Object Notation}
    \item XML\footnote{eXtensible Markup Language}
\end{itemize}

We can implement both solutions and let the client decide what representation
he would like to work with. When a client sends a request, it specifies the
MIME\footnote{Multipurpose Internal Mail Extension -
http://tools.ietf.org/html/rfc2046} type of the representation that he wants
(i.e. 'application/json') in the Accept field of the HTTP header.

The same can be done by using the Content-Type field in the HTTP header. We
can specify the MIME type of the request we are sending and the server can
parse the request based on that.

However, for this API we will restrict the user to only use JSON for the
sake of simplicity. JSON is also a lighter format and it's easier to parse
which makes it a better choice.

The Flask application will assume that the request is JSON encoded and just
return a Bad Request (400 HTTP status code) response if this is not the case.

\subsection{Message Format}

Each message has the following generic JSON representation :
\begin{listing}
    \begin{minted}{javascript}
response = {
    'result': result, // this is different based on the request
    'errorCode': code // if the errorCode is 0 then the request
                      // was successful
                      // otherwise, result contains a readable
                      // error message
};
    \end{minted}
    \caption{Message format}
    \label{lst:api01}
\end{listing}

We also use the HTTP status codes meaning when returning a response.
For example, we return a 204 status code (HTTP status for No Content) when
we get a request for the update of a resource when the action is successful
because that's the only piece of information relevant to the response.

Using HTTP status codes correctly can also help clients of the API by
they can assume that seeing a 201 status code (Created) actually means
that a new resource was created (in this particular case, a link to the
new resource is also included)

\subsection{Stateless}

Our API is stateless. That means that the server does not keep client
context. Each request must contain all the information required for it to be
processed.

\subsubsection{Authorization}

Authorization data should not be part of the payload. But we need to include it
with every request to maintain our goal of statelessness.

The solution to this problem is HTTP Basic Authentication. Each request that
requires authorization simply includes the Authorization header which contains
a Base64\footnote{https://tools.ietf.org/html/rfc4648} encoding of the string
"username:password". This provides no encryption or authentication and should
only be used bare when on a secure network. If the network is insecure, we can
couple it with SSL/TLS (Secure Socket Layer/Transport Layer Security) to provide
encryption and authentication.

\subsubsection{Impact on Server Load}

Not keeping context in the server implies that every request is self contained.
This means that we must supply all the context necessary for a request to be
processed. This can incur the use of more bandwidth as there is more payload
to transfer.

We solve this issue by using the IDs allocated by the RDBMS for each resource
rather than more verbose identification means (such as names). This does,
however, increase the load on the server because it has to query the
database to find the resource identified by the IDs included in the request
but we trade-off a slight performance hit for increased memory usage that
concurrent login sessions impose.
