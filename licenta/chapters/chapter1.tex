\chapter{Introduction}

Programming assignments are a simple and effective way of assessing the level
of understanding of a student in the context of a Computer Science course.
A well thought out assignment can expose the areas that the student needs to
focus on to improve and can speed up learning by a great margin. That makes the
feedback that a student receives for a submission very important.

A part of the feedback consists of verifying that a certain assignment correctly
solves the problem. Thus, grading a submission consists of running a set of
tests on the submission and verifying the output against a reference output.

Grading a submission typically consists of the following steps:
\begin{enumerate}
    \item run a set of tests to verify correctness
    \item go over the submission to check for other metrics such as
        coding style, programming idioms, interface design, etc.
    \item give personalized feedback based on observations from
        the previous step
\end{enumerate}

While steps two and three can be hard to automate, step one is a perfect
candidate for automation. Automating the tests that verify the correctness of
a submission speed up the grading process and provide fairness (all the
submissions are verified in the same way).

In this work we extend the functionality of \textbf{vmchecker}, a tool designed
to solve the problems stated above. The extension consists of an external
API\footnote{Application Programming Interface} that will allow other tools
to communicate with and use the \textbf{vmchecker} automated submission
grader.

\section{vmchecker}

\textbf{vmchecker} is an automated submission grader designed to help both
students and teachers/teaching assistants in providing a better and fairer
experience for students while enabling them to grade a large amount of
homeworks.

The core of \textbf{vmchecker} is written in Python and its web services
are provided using the \textbf{mod\_python} extension of the
\textbf{Apache HTTP Server}.

The user interface is written using \textbf{Google Web Toolkit} and uses
the web services to communicate with the rest of vmchecker.

\subsection{Workflow}

When a user uploads a submission to \textbf{vmchecker} for grading, the
submission is stored and sent to a testing queue. Then, the
submission is run in a virtual environment and the results are made
available for the student.

\begin{figure}[h!]
    \caption{vmchecker workflow for a submission}
    \centering
% TODO include dia instead of png
        \includegraphics[scale=0.35]{fig01}
    \label{fig:vmchecker01}
\end{figure}

Figure \ref{fig:vmchecker01} represents the flow of the submission through
the \textbf{vmchecker} core from the moment it is uploaded until the results are
available.

\subsection{Architecture}

Courses usually have at least 50 students so every assignment can have
a lot of submissions queued for testing at any single moment. The tests ran to
check every submission can also be very demanding from a computational
perspective and, as such, each tester handles one submissions at a given time.

In order to scale with the number of students, \textbf{vmchecker} is broken down
into 2 separate parts:
\begin{itemize}
    \item \textbf{storers}
    \item \textbf{testers}
\end{itemize}

\subsection{Storers}

The storer is the \textbf{vmchecker} component that handles the
storing of configuration files and submissions.

Each \textbf{storer} provides a web interface that is used by students to upload
submissions for grading. The tests that are run for each submission
are also located on the \textbf{storer}.

\begin{listing}[h!]
    \dirtree{%
    .1 /.
    .2 config\DTcomment{course configuration file}.
    .2 auth\_file.json\DTcomment{login information}.
    .2 repo\DTcomment{base location for submissions}.
    .3 assignment\_name\DTcomment{submissions for a particular assignment}.
    .4 username\DTcomment{submissions for a particular user}.
    .5 current\DTcomment{link to the last submission sent by the user}.
    .5 sb\_10.25\_31.08.2015\_rnd23\DTcomment{a particular submission}.
    .6 archive.zip\DTcomment{archive of the submission}.
    .6 git\DTcomment{content of the submission archive}.
    .2 tests\DTcomment{tests for the available assignments}.
    .3 assignment\_name.zip\DTcomment{tests for an assignment}.
    }
    \caption{The directory structure of a course on the storer}
    \label{lst:vmchecker01}
\end{listing}

Listing \ref{lst:vmchecker01} presents the folder structure of a course on the
\textbf{storer}. Each course has a repository where all the submissions are
stored.
The repository first level represents the assignments and the second level
represents the user. All submissions are saved and the symbolic link
\texttt{current} always points to the location of the last submission a user
uploaded for a certain assignment.

When a student uploads a submission, it is saved on the \textbf{storer} and a
submission bundle is created and sent to the \textbf{tester} defined in the
configuration file and enqueued there.

\subsection{Testers}

When a submission bundle is dequeued, the bundle is unpacked and the
submission configuration is read. This configuration contains all the
necessary information the \textbf{tester} needs to start up the virtual
environment and run the tests.

The use of a virtual machine as an environment for the testing of the
submissions provides isolation and improves fairness (each homework
is tested in the same environment). The virtual machine can also be
made available to the student in order to minimize the differences
between the testing environment and the environment the student is
working in.

However, starting up a virtual machine incurs a high computational
load and, as such, rather than overcommit available resources and risk
reducing the fairness of the evaluation, we only test one submission at
a time on a particular \textbf{tester}.

After the tests are ran, the results are picked up by a daemon residing on
the storer that queued the submission.

\section{External API}

\textbf{vmchecker} provides an API\footnote{Application Programming Interface}
that can be used to send submissions and get results programmatically.

The API lacks any methods to add new courses or assignments or to update any
kind of configuration. This can only be done by logging into the storer
and manually editing the configuration files. Grading is done the same way.

To prevent unauthorized access, each course uses its own UNIX user. However, the
web services need access to the repository to store the submissions for a
certain assignments. Because the web services are called using
the \textbf{mod\_python} extension of the web server, they run using
the same UNIX user as the web server. This means that this user will also need
access to the repository of the courses and the problem is solved by using
access control lists (ACLs). This complicates the setup of the system.

\section{Improving vmchecker - A RESTful API}

A better API can open doors for the improvement of \textbf{vmchecker} as it
could provide 3rd-party developers with the possibility of adding additional
functionality.

\subsection{WSGI}

WSGI\footnote{Web Server Gateway Interface} is "a standard interface
between web servers and Python web applications or frameworks, to promote
web application portability." \cite{pep333}.

WSGI provides a way of writing web applications that don't depend on the
web server. This allows us the flexibility of choosing any web server that
implements WSGI and doesn't lock us into using a single implementation.

\subsection{REST}

"REST\footnote{REpresentational State Transfer} is an architectural
style consisting of software engineering principles and
the interaction constraints chosen to retain those principles."
\cite{fielding2000}

The constraints and guidelines that are highly useful for a RESTful API are
the following:
\begin{itemize}
    \item client-server: separating the internal representation (the one on
        the server) from the external one (the one the client sees).
    \item stateless: each request is self-contained and needs no additional
        context in order to be processed.
    \item cache: "data within a response to a request can be implicitly or
        explicitly labeled as cacheable or non-cacheable"\cite{fielding2000}.
        The client is free to determine whether it can use the cached version
        based on this label.
    \item layered system: the system can be designed as a set of layers that
        can provide various functions (such as authorization or caching) and
        "by restricting the client knowledge to a single layer, we promote
        substrate independence"\cite{fielding2000}.
\end{itemize}
