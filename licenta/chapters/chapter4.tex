\chapter{API Clients}

We now present two clients that consume the new API:
\begin{itemize}
    \item CLI\footnote{Command Line Interface} client
    \item Moodle plugin for vmchecker
\end{itemize}

The CLI client can be used to simplify the usage of vmchecker by
providing an interface that can be used inside a terminal. This can help speed
up some actions such as submitting homeworks or adding courses/assignments.

The Moodle plugin is an extension of the Moodle online learning platform that
allows Moodle to send the submissions sent in by students to vmchecker for
testing and grading.

\section{Command Line Interface Client}

A CLI client for vmchecker is a tool that can speed up both student and
teacher/teaching assistant workflow by removing the need of switching to
a web browser to upload a submission or create a new course/assignment.

The client can also serve for automation as it can be easily integrated
in shell scripts.

We will implement the client in \textbf{Python} using \textbf{Click} and
\textbf{Requests}.

\subsection{Architecture}

The client will expose every resource as a command and every action as
a subcommand:

\begin{listing}
    \begin{minted}{python}
./cli.py RESOURCE COMMAND [OPTIONS] [ARGUMENTS]
    \end{minted}
    \caption{CLI usage example}
    \label{lst:cli01}
\end{listing}

Each resource is exposed as the main argument to our client. The next argument
is one of the available actions for the specified resource.

The options and arguments that follow after that are specific to the resource
and action given as the first two arguments.

\subsection{Commands}

In \textbf{Click}, a command is defined as follows:
\begin{listing}
    \begin{minted}[linenos=true]{python}
import click
from click import STRING

@click.command
@click.option('--name', type=STRING, required=True)
def add_course(*args, **kw_args):
    # code to add a new course
    \end{minted}
    \caption{Defining a command for the CLI using Click}
    \label{lst:cli02}
\end{listing}

This is all that is needed to expose a new CLI command on our client.

\subsection{Using the API}

To actually add a new course, we need to call the API. Since we expose it
through HTTP, a call is an HTTP request. Let's see how we can add a new
course using the \textbf{Requests} package in Python.

\begin{listing}
    \begin{minted}[linenos=true]{python}
import simplejson as json
import requests
from requests.auth import HTTPBasicAuth

def add_course(name):
    auth=HTTPBasicAuth('admin', '123456')
    url='http://localhost:5000/api/courses/'
    response = requests.posts(url, data=json.dumps({'name': name},
        auth=auth)
    return response.json()
    \end{minted}
    \caption{Calling the external API using the Requests package}
    \label{lst:cli03}
\end{listing}

This is it. Using the above code we create a new course. All that we need
to do is to send a HTTP POST request to the courses API endpoint
and include in the payload JSON containing the course name.

\subsection{Usefulness}

Right now, the command line client is rather crude. It exposes all the
actions as add, edit and delete. Grading a submission, although being
an edit action, can be exposed to the user as a grade action that takes
as argument the grade and, optionally, the comments.

Although this client is simply created as a proof of concept, it can serve
as the basis for a more complex client that can actually improve the workflow
of using vmchecker.

Most of the people using it are students that need to submit their homework for
grading and a CLI client that lets them do that from their favorite terminal
would greatly improve the workflow.

The client can also help the people grading the submissions by allowing them to
download one or more submissions and grade them without having to log into the
storage server.

\section{Moodle plugin for vmchecker}

\textbf{Moodle} is an open-source learning platform. It is designed as an
online companion for the available courses and supports a lot of the
workflow associated with such courses (such as posting assignments and
grading and publishing submission results and posting course slides
and necessary content).

\subsection{Problem}

Although Moodle has support for posting and grading assignments, grading them
remains a tedious task. It helps by aggregating all the submission in the same
interface but the grading process is still pretty much manual. If only there was
some automated submission grader that can be used to provide automation for this
task.

What about vmchecker? It can automatically grade submissions based on a set of
tests. We could use that for Moodle. All we need to do is to create a new plugin
that sends the submissions to vmechecker for testing and then gets back the
results and makes them available for the students.

\subsection{Plugin}

Moodle offers a simple way for developers to extend it by the use of various
types of plugins. For our use case, we will create a plugin called an observer.

Every time a user uploads his submission for the assignment, the plugin kicks
in and checks whether that submission should be sent to vmchecker for automated
testing and, if it does, it sends it using the API.

\subsection{Web Service}

Because the testing of an assignment can, potentially, take quite a lot of time,
the request for grading must be asynchronous. That raises the problem of how
we can get back the results from vmchecker.

One solution would be to poll vmchecker every time the student accesses the
submission page on Moodle. However, this would introduce aditional load on
both vmchecker and Moodle every time this happens. And this can, potentially,
happen quite a lot because students tend to be anxious to see the result of
the grading.

The other solution is by using a push notification. When Moodle sends the
submission to vmchecker for grading, it includes a token and callback data
to authorize and identify vmchecker and permit the posting of the results.
We just need to expose a Moodle web service that allows the grading of a
submission. This approach is much lighter than the polling approach and
is perfect for our workflow.

\subsection{Transfered Data}

Each submission sent to vmchecker needs the following information:
\begin{itemize}
    \item assignment ID
    \item user ID
\end{itemize}

The submissions are categorized based on these 2 values. In order to
use vmchecker, the course needs to be setup and an assignment needs
to be added to it. But this is also the case with Moodle. Moodle
also has assignment IDs and user IDs.

In order to use it with vmchecker, we provide a mapping for the
assignments to map assignment ID from Moodle to the assignment ID
from vmchecker.

User IDs can't really benefit from this approach
because they are setup more dynamically (for example, LDAP is consulted
and a user is created on the fly). As such, our approach is to
use a generic user for Moodle on vmchecker. This means that all Moodle
submissions are viewed from vmchecker as being from a single user
(they can be identified by checking the callback data) but we no longer
need to map users between the two systems.
