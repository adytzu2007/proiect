# -*- mode: ruby -*-
# vi: set ft=ruby :

$apt_repositories = <<SCRIPT
apt-get update
SCRIPT

VAGRANTFILE_API_VERSION = "2"

unless Vagrant.has_plugin?("vagrant-omnibus")
    raise "Omnibus plugin not installed"
end

VAGRANTFILE_BOX_NAME = "trusty-x86_64"
VAGRANTFILE_BOX_URL  = "https://cloud-images.ubuntu.com/vagrant/trusty/current/trusty-server-cloudimg-amd64-vagrant-disk1.box"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
    config.vm.box = VAGRANTFILE_BOX_NAME
    config.vm.box_url = VAGRANTFILE_BOX_URL

    # network configuration
    config.vm.network :forwarded_port, host: 8080, guest: 80
    config.vm.network :forwarded_port, host: 5432, guest: 5432

    # provisioning
    # get latest chef version
    config.omnibus.chef_version = :latest

    config.vm.provision "shell", inline: $apt_repositories

    config.vm.provision "chef_solo" do |chef|
        chef.json = {
            "postgresql" => {
                "users" => [{
                    "username" => "root",
                    "password" => "root",
                    "superuser" => true,
                    "createdb" => true,
                    "login" => true
                }],
                "databases" => [{
                    "name" => "moodle",
                    "owner" => "root",
                    "template" => "template0",
                    "encoding" => "utf8",
                    "locale" => "en_US.UTF8"
                }],
                "pg_hba" => [{
                    "type" => "host",
                    "db" => "all",
                    "user" => "root",
                    "addr" => "0.0.0.0/0",
                    "method" => "md5"
                }],
                "listen_addresses" => "*"
            },
            "apache" => {
                "user" => "vagrant",
                "group" => "vagrant",
                "default_site_enabled" => true
            }
        }

        chef.add_recipe "postgresql::client"
        chef.add_recipe "postgresql::server"
        chef.add_recipe "apache2"
        chef.add_recipe "php::package"
        chef.add_recipe "php::module_pgsql"
        chef.add_recipe "php::module_curl"
        chef.add_recipe "php::module_gd"
        chef.add_recipe "apache2::mod_php5"
    end

    config.vm.provision "shell", path: "start.sh"
end

