#!/bin/bash

#############################################
#
# functions
#
############################################
# clone permissions of the current user to a different user
function clone_permissions()
{
    if [ ! $# -eq 3 ]; then
        echo "Usage: clone_permissions target user group"
        return 1
    fi

    target=$1
    user=$2
    group=$3

    for permissions in $(getfacl ${target} | grep -v "^#"); do
        who=$(echo ${permissions} | cut -f1 -d':')
        mode=$(echo ${permissions} | cut -f3 -d':')
        if [ "${who}" = "user" ]; then
            setfacl -m "u:${user}:${mode}" ${target}
        elif [ "${who}" = "group" ]; then
            setfacl -m "g:${group}:${mode}" ${target}
        fi
    done

    return 0
}

function clone_dir_permissions()
{
    if [ ! $# -eq 3 ]; then
        echo "Usage: clone_dir_permissions dir user group"
        return 1
    fi

    target_dir=$1
    user=$2
    group=$3

    for target in $(find ${target_dir}); do
        clone_permissions ${target} ${user} ${group}
    done

    return 0
}

SERVICE=/usr/sbin/service

##############################################
# shell provisioning for moodle + vmchecker
#
##############################################
apt-get install -y acl

#######################################
# moodle configuration
#
#######################################
# copy moodle sources
ln -s /vagrant/moodle /var/www/moodle

# create moodle data directory
mkdir -p /var/wwwdata/moodle
chown -R vagrant.vagrant /var/wwwdata /var/www

# create moodle entry in cron
echo "*/5 * * * * vagrant /usr/bin/php /var/www/moodle/admin/cron.php" >> /etc/crontab

SERVICES="apache2 postgresql"

#######################################
# vmchecker configuration
#
#######################################
if [[ "$install_vmchecker" = "true" ]]; then
    apt-get install -y python-paramiko python-pyinotify python-pkg-resources
    apt-get install -y python-ldap acl
    TMPDIR=$(mktemp -d)

    # VMware VIX
    VMWARE_VIX_VERSION="1.6.2-127388"
    VMWARE_VIX_ARCHIVE="/vagrant/VMware-vix-${VMWARE_VIX_VERSION}.x86_64.tar.gz"
    tar -xf ${VMWARE_VIX_ARCHIVE} -C ${TMPDIR}

    # get rid of pesky EULA agreement
    sed -i -r 's/(sub show_EULA \{)/\1\n  return;\n/g' ${TMPDIR}/vmware-vid-distrib/vmware-install.pl
    ${TMPDIR}/vmware-vix-distrib/vmware-install.pl --default

    PYVIX_REPO="git://github.com/luciang/pyvix.git"
    apt-get install -y git
    apt-get install -y python2.7-dev
    git clone ${PYVIX_REPO} ${TMPDIR}/pyvix
    python ${TMPDIR}/pyvix/setup.py install

    # install vmchecker
    echo "Copying vmchecker sources..."
    ${RM} -rf ~/vmchecker
    ${CP} -r /vagrant/vmchecker ~
    cd ~/vmchecker
    python setup.py install

    useradd -m test
    su - test
    mkdir storer
    cd storer
    vmchecker-init-course storer
    vmchecker-init-course tester
    cd tests
    wget http://swarm.cs.pub.ro/~vgosu/vmchecker/1-minishell-linux.zip
    wget http://swarm.cs.pub.ro/~vgosu/vmchecker/1-minishell-windows.zip
    cd ..
    cat > auth_file.json <<EOF
    {
        "auth" : { "user1" : "password1" }
    }
EOF
    cd /var/www
    wget http://swarm.cs.pub.ro/~vgosu/vmchecker/ui.tar.gz
    tar -xf ui.tar.gz
    rm -f ui.tar.gz

    SERVICES+=" vmchecker"
fi

# start services
for service_name in ${SERVICES}; do
    if ! ${SERVICE} ${service_name} status > /dev/null; then
        echo "Started service ${service_name}"
        ${SERVICE} ${service_name} start > /dev/null;
    else
        echo "Service ${service_name} already started"
    fi
done

