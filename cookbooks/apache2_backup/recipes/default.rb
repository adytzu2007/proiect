#
# Cookbook Name:: apache2
# Recipe:: default
#
# Copyright 2008-2013, Opscode, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

package 'apache2' do
  package_name node['apache']['package']
end

service 'apache2' do
  service_name 'apache2'
  restart_command '/usr/sbin/invoke-rc.d apache2 restart && sleep 1'
  reload_command '/usr/sbin/invoke-rc.d apache2 reload && sleep 1'
  supports [:restart, :reload, :status]
  action [ :enable, :stop ]
end

directory node['apache']['log_dir'] do
  mode '0755'
end

package 'perl'

%w[sites-available sites-enabled mods-available mods-enabled conf-available conf-enabled].each do |dir|
  directory "#{node['apache']['dir']}/#{dir}" do
    mode  '0755'
    owner 'root'
    group node['apache']['root_group']
  end
end

template "/usr/sbin/a2enmod" do
  source "a2enmod.erb"
  mode  '0700'
  owner 'root'
  group node['apache']['root_group']
end

%W[
  #{node['apache']['dir']}/ssl
  #{node['apache']['dir']}/conf-available
  #{node['apache']['cache_dir']}
].each do |path|
  directory path do
    mode  '0755'
    owner 'root'
    group node['apache']['root_group']
  end
end

template 'apache2.conf' do
  path     "#{node['apache']['dir']}/apache2.conf"
  source   'apache2.conf.erb'
  owner    'root'
  group    node['apache']['root_group']
  mode     '0644'
end

template 'envvars' do
  path     "#{node['apache']['dir']}/envvars"
  source   'envvars.erb'
  owner    'root'
  group    node['apache']['root_group']
  mode     '0644'
end

template 'apache2-conf-security' do
  path     "#{node['apache']['dir']}/conf-available/security.conf"
  source   'security.erb'
  owner    'root'
  group    node['apache']['root_group']
  mode     '0644'
  backup   false
end

template "#{node['apache']['dir']}/ports.conf" do
  source   'ports.conf.erb'
  owner    'root'
  group    node['apache']['root_group']
  mode     '0644'
end

template "#{node['apache']['dir']}/sites-available/000-default.conf" do
  source   '000-default.conf.erb'
  owner    'root'
  group    node['apache']['root_group']
  mode     '0644'
end

node['apache']['default_modules'].each do |mod|
  module_recipe_name = mod =~ /^mod_/ ? mod : "mod_#{mod}"
  include_recipe "apache2::#{module_recipe_name}"
end

apache_site 'default' do
  enable node['apache']['default_site_enabled']
end

service 'apache2' do
  action :start
end
