#
# Cookbook Name:: apache2
# Attributes:: apache
#
# Copyright 2008-2013, Opscode, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

default['apache']['root_group'] = 'root'

# Where the various parts of apache are
default['apache']['package']     = 'apache2'
default['apache']['dir']         = '/etc/apache2'
default['apache']['log_dir']     = '/var/log/apache2'
default['apache']['error_log']   = 'error.log'
default['apache']['access_log']  = 'access.log'
default['apache']['user']        = 'www-data'
default['apache']['group']       = 'www-data'
default['apache']['binary']      = '/usr/sbin/apache2'
default['apache']['docroot_dir'] = '/var/www'
default['apache']['cgibin_dir']  = '/usr/lib/cgi-bin'
default['apache']['icondir']     = '/usr/share/apache2/icons'
default['apache']['cache_dir']   = '/var/cache/apache2'
default['apache']['pid_dir']     = '/var/run'
default['apache']['pid_file']    = "#{node['apache']['pid_dir']}/apache2.pid"
default['apache']['lock_dir']    = '/var/lock/apache2'
default['apache']['lib_dir']     = '/usr/lib/apache2'
default['apache']['libexecdir']  = "#{node['apache']['lib_dir']}/modules"
default['apache']['default_site_enabled'] = false

###
# These settings need the unless, since we want them to be tunable,
# and we don't want to override the tunings.
###

# General settings
default['apache']['listen_addresses']  = %w[*]
default['apache']['listen_ports']      = %w[80]
default['apache']['contact']           = 'ops@example.com'
default['apache']['timeout']           = 300
default['apache']['keepalive']         = 'On'
default['apache']['keepaliverequests'] = 100
default['apache']['keepalivetimeout']  = 5

# Security
default['apache']['servertokens']    = 'Prod'
default['apache']['serversignature'] = 'On'
default['apache']['traceenable']     = 'On'

# mod_auth_openids
default['apache']['allowed_openids'] = []

# mod_status Allow list, space seprated list of allowed entries.
default['apache']['status_allow_list']['hosts'] = 'localhost ip6-localhost'

# mod_status ExtendedStatus, set to 'true' to enable
default['apache']['ext_status'] = false

# mod_info Allow list, space seprated list of allowed entries.
default['apache']['info_allow_list']['ips']   = ''
default['apache']['info_allow_list']['hosts'] = 'localhost ip6-localhost'

# Prefork Attributes
default['apache']['prefork']['startservers']           = 16
default['apache']['prefork']['minspareservers']        = 16
default['apache']['prefork']['maxspareservers']        = 32
default['apache']['prefork']['serverlimit']            = 400
default['apache']['prefork']['maxrequestworkers']      = 400
default['apache']['prefork']['maxconnectionsperchild'] = 10_000

# Worker Attributes
default['apache']['worker']['startservers']           = 4
default['apache']['worker']['serverlimit']            = 16
default['apache']['worker']['maxclients']             = 1024
default['apache']['worker']['minsparethreads']        = 64
default['apache']['worker']['maxsparethreads']        = 192
default['apache']['worker']['threadsperchild']        = 64
default['apache']['worker']['maxconnectionsperchild'] = 0

# mod_proxy settings
default['apache']['proxy']['deny']['ips']    = ''
default['apache']['proxy']['deny']['hosts']  = ''
default['apache']['proxy']['allow']['ips']   = ''
default['apache']['proxy']['allow']['hosts'] = ''

# Default modules to enable via include_recipe

default['apache']['default_modules'] = %w[
  status alias auth_basic authn_file authz_groupfile authz_host authz_user autoindex
  dir env mime negotiation setenvif
]

%w[log_config logio].each do |log_mod|
  default['apache']['default_modules'] << log_mod if %w[rhel fedora suse arch freebsd].include?(node['platform_family'])
end
